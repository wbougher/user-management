# User Profiles

Task 1  
1. Install node JS on your local machine => DONE  
2. Use the Express node framework and MongoDB to create a one page web app that displays a user list => DONE  
    a. Create a login page => DONE  
    b. Display the user list on the main page after login. => DONE  
        Bonus: add a form here to create a new user => DONE  
c. Store the users and encrypt their password in the mongoDB instance => DONE  
3. Test user login and password encrypt/decrypt so that you can confirm that it is working as intended => DONE