const express = require('express');
const router = express.Router();
const { ensureAuthenticated } = require('../config/auth');

// User model
const User = require('../models/User');

// Welcome page
router.get('/', (req, res) => res.render('welcome'));

// Dashboard
router.get('/dashboard', ensureAuthenticated, (req, res) => {
    User.find({}).exec((err, users) => {
        if (err) throw err;
        res.render('dashboard', { name: req.user.name, "users": users });
    })
});

module.exports = router;